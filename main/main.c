/*
 * File:   lightauto.c
 * Author: DELL
 *
 * Created on August 7, 2020, 9:20 PM
 */
//  Microchip PIC16F887 Configuration pragmas

//  Oscillator Selection bits
//  #pragma config "FOSC=EXTRC_CLKOUT"  // RC oscillator
//  #pragma config "FOSC=EXTRC_NOCLKOUT"// RCIO oscillator
//  #pragma config "FOSC=INTRC_CLKOUT"  // INTOSC oscillator
//  #pragma config "FOSC=INTRC_NOCLKOUT"// INTOSCIO oscillator
//  #pragma config "FOSC=EC"            // EC
//  #pragma config "FOSC=HS"            // HS oscillator
//  #pragma config "FOSC=XT"            // XT oscillator
//  #pragma config "FOSC=LP"            // LP oscillator

//  Watchdog Timer Enable bit
//  #pragma config "WDTE=ON"            // WDT enabled
//  #pragma config "WDTE=OFF"           // WDT disabled and can be enabled by SWDTEN bit of the WDTCON register

//  Power-up Timer Enable bit
//  #pragma config "PWRTE=OFF"          // PWRT disabled
//  #pragma config "PWRTE=ON"           // PWRT enabled

//  RE3/MCLR pin function select bit
//  #pragma config "MCLRE=ON"           // RE3/MCLR pin function is MCLR
//  #pragma config "MCLRE=OFF"          // RE3/MCLR pin function is digital input, MCLR internally tied to VDD

//  Code Protection bit
//  #pragma config "CP=OFF"             // Program memory code protection is disabled
//  #pragma config "CP=ON"              // Program memory code protection is enabled

//  Data Code Protection bit
//  #pragma config "CPD=OFF"            // Data memory code protection is disabled
//  #pragma config "CPD=ON"             // Data memory code protection is enabled

//  Brown Out Reset Selection bits
//  #pragma config "BOREN=ON"           // BOR enabled
//  #pragma config "BOREN=NSLEEP"       // BOR enabled during operation and disabled in Sleep
//  #pragma config "BOREN=SBODEN"       // BOR controlled by SBOREN bit of the PCON register
//  #pragma config "BOREN=OFF"          // BOR disabled

//  Internal External Switchover bit
//  #pragma config "IESO=ON"            // Internal/External Switchover mode is enabled
//  #pragma config "IESO=OFF"           // Internal/External Switchover mode is disabled

//  Fail-Safe Clock Monitor Enabled bit
//  #pragma config "FCMEN=ON"           // Fail-Safe Clock Monitor is enabled
//  #pragma config "FCMEN=OFF"          // Fail-Safe Clock Monitor is disabled

//  Low Voltage Programming Enable bit
//  #pragma config "LVP=ON"             // RB3/PGM pin has PGM function, low voltage programming enabled
//  #pragma config "LVP=OFF"            // RB3 pin has digital I/O, HV on MCLR must be used for programming

//  In-Circuit Debugger Mode bit
//  #pragma config "DEBUG=OFF"          // In-Circuit Debugger disabled, RB6/ICSPCLK and RB7/ICSPDAT are general purpose I/O pins
//  #pragma config "DEBUG=ON"           // In_Circuit Debugger enabled, RB6/ICSPCLK and RB7/ICSPDAT are dedicated to the debugger

//  Brown-out Reset Selection bit
//  #pragma config "BOR4V=BOR21V"       // Brown-out Reset set to 2.1V
//  #pragma config "BOR4V=BOR40V"       // Brown-out Reset set to 4.0V

//  Flash Program Memory Self Write Enable bits
//  #pragma config "WRT=HALF"           // 0000h to 0FFFh write protected, 1000h to 1FFFh may be modified by EECON control
//  #pragma config "WRT=1FOURTH"        // 0000h to 07FFh write protected, 0800h to 1FFFh may be modified by EECON control
//  #pragma config "WRT=256"            // 0000h to 00FFh write protected, 0100h to 1FFFh may be modified by EECON control
//  #pragma config "WRT=OFF"            // Write protection off

#include<xc.h>
#include<pic.h>
#include"lcd.h"
#include <stdlib.h> 
#include <stdio.h>
#define rs RC0
#define rw RC1
#define e RC2
#define BULB1 RE0
#define BULB2 RE1
#define pir1 RC6
#define pir2 RC7
 
__CONFIG( FOSC_HS & WDTE_OFF & PWRTE_OFF & CP_OFF & BOREN_ON & LVP_OFF & CPD_OFF & WRT_OFF & DEBUG_OFF);
 
int count;
char array[10];
char h[55];
unsigned int val;
unsigned char rec;
unsigned char sec,min,hour,date,month,year;
unsigned int adc();
void rtc_int();
void rtc_start();
void rtc_stop();
void rtc_ack();
void rtc_nak();
void rtc_res();
void rtc_send(unsigned char a);
void rtc_send_byte(unsigned char addr,unsigned char data);
unsigned char rtc_read();
unsigned char rtc_read_byte(unsigned char addr);
void waitmssp();
unsigned char convup(unsigned char bcd);
unsigned char convd(unsigned char bcd);

void pircheck(){
    
    if(pir1 ==1 && pir2 == 0 && count >0){
        BULB1=1;
        count++; 
        sprintf(h, "MotionDin:  %d", count);        
        show(h);
        
    }
    
    else if(pir1 ==0 && pir2 == 1 && count >0){
        BULB1=1;
        count--; 
        sprintf(h, "MotionDout:  %d", count);        
        show(h);
    }
    
    if(count==0){
        BULB2=0;
    }
    else{
        BULB2=1;
    }
    
}

void ldrcheck(){
    unsigned int val;
    val = adc();    
    
    if(val>150 ) {
            
            show(" LOW");
            BULB1 = 1;
            
        } else {
            
            show("HIGH");
            BULB1 = 0;
        }
}

void main()
{
    
    TRISE0 = 0;
    TRISE1=0;
    TRISA0=1; 
    TRISC6=1; 
    TRISC7=1; 
    
    lcd_init();
    
    show("Time:");
    cmd(0xc0);
    show("Date:");
    cmd(0x90);
    show("LIGHT INT:");
    rtc_int();
   
    while(1) {
        sec  =rtc_read_byte(0x00);
        min  =rtc_read_byte(0x01);
        hour =rtc_read_byte(0x02);
        date =rtc_read_byte(0x04);
        month=rtc_read_byte(0x05);
        year =rtc_read_byte(0x06);
        
        cmd(0x85);
        dat(convup(hour));
        dat(convd(hour));
        dat(':');
        dat(convup(min));
        dat(convd(min));
        dat(':');
        dat(convup(sec));
        dat(convd(sec));
        
        cmd(0xc5);
        dat(convup(date));
        dat(convd(date));
        dat(':');
        dat(convup(month));
        dat(convd(month));
        dat(':');
        dat(convup(year));
        dat(convd(year));
        cmd(0x9A);
        ldrcheck();
        
        cmd(0xD0);
        pircheck();
        
        }
   
    
    }
        
        
        
            

    
      
    
 
void rtc_int()
{
    TRISC3=TRISC4=1;
    SSPCON=0x28;
    SSPADD= (((11059200/4)/100)-1);
}
 
void waitmssp()
{
    while(!SSPIF); // SSPIF is zero while TXion is progress    
    SSPIF=0;
}
 
void rtc_send(unsigned char a)
{
    SSPBUF=a;
    waitmssp();
    while(ACKSTAT);
}   
 
void rtc_send_byte(unsigned char addr,unsigned char data)
{
    rtc_start();
    rtc_send(0xD0);
    //rtc_send(addr>>8);
    rtc_send(addr);
    rtc_send(data);
    rtc_stop();
}
 
unsigned char rtc_read()
{
    RCEN=1;
    waitmssp();
    return SSPBUF;
}   
    
unsigned char rtc_read_byte(unsigned char addr)
{
    unsigned char rec;
L:  rtc_res();
    SSPBUF=0xD0;
    waitmssp();
    while(ACKSTAT){goto L;}
    //rtc_send(addr>>8);
    rtc_send(addr);
    rtc_res();
    rtc_send(0xD1);
    rec=rtc_read();
    rtc_nak();
    rtc_stop();
    return rec;
}
    
    
void rtc_start()
{
    SEN=1;
    waitmssp();
}
 
void rtc_stop()
{
    PEN=1;
    waitmssp();
}
 
void rtc_res()
{
    RSEN=1;
    waitmssp();
}
 
void rtc_ack()
{
    ACKDT=0;
    ACKEN=1;
    waitmssp();
}
 
void rtc_nak()
{
    ACKDT=1;
    ACKEN=1;
    waitmssp();
}
 
unsigned char convup(unsigned char bcd)
{ 
    return ((bcd>>4)+48);
}
 
unsigned char convd(unsigned char bcd)
{ 
    return ((bcd&0x0F)+48);
}


unsigned int adc()
{
    unsigned int adcval;
    
    ADCON1=0xc0;                    //right justified
    ADCON0=0x85;                    //adc on, fosc/64
    while(GO_nDONE);                //wait until conversion is finished
    adcval=((ADRESH<<8)|(ADRESL));    //store the result
    adcval=(adcval/3)-1;
        
    return adcval;  
}Main Code